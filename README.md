# phone book to Gigaset Dect

Convert the phone book from mailbox.org to a Gigaset Dect phone.

## Export phonebook

Login into https://login.mailbox.org navigate to phone book, select contacts, check the checkbox for all contacts, choose hamburger menu (three dashes) above and then export. As format choose vCard and export. Save the file to the directory of your choice. The default directory is often the ~/download directory. 

## Use Kontakt for converting

The KDE software Kontact has a (helper) software called kaddressbook. Import the file from mailbox.org and export it as vCard 2.1. I didn't export encryption keys and images.

## Converting Umlaute (äöü)

My Gigaset didn't recognize the Umlaute (äöü) and I converted them to plain text with [konvertiere-umlaute-in-vcf.sh](https://gitlab.com/JKorte/phone-book-to-gigaset-dect/-/blob/main/konvertiere-umlaute-in-vcf.sh). I just added the Umlaute I got.

## Upload the file

Login into the webinterface of your base station, select the phone and upload the converted file.
