#!/bin/bash
# (C) AGPL 3.0
#
# Adressen aus mailbox.org exportieren, mit KAdressbook von Kontact exportieren nach VCF 2.1 und dann dieses Script und hochladen ins Gigaset


# Fixme: Ä und Ü fehlen, was ist mit ß ?

cat addressbook.vcf | sed -e "s/=C3=A4/ä/g"  -e "s/=C3=B6/ö/g" -e "s/=C3=BC/ü/g" -e "s/=C3=96/Ö/g" > addressbook-umlaute.vcf
